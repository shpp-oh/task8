curl -u admin:admin -X POST --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons" -H "Content-Type: application/json" -d "{\"id\": 0, \"firstName\": \"firstName1\", \"lastName\": \"lastName1\", \"ipn\": \"1111111118\"}"
echo -e
curl -u admin:admin -X POST --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons" -H "Content-Type: application/json" -d "{\"id\": 0, \"firstName\": \"firstName2\", \"lastName\": \"lastName2\", \"ipn\": \"2222222225\"}"
echo -e
curl -u admin:admin -X POST --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons" -H "Content-Type: application/json" -d "{\"id\": 0, \"firstName\": \"firstName3\", \"lastName\": \"lastName3\", \"ipn\": \"3333333332\"}"
echo -e
curl -u admin:admin -X POST --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons" -H "Content-Type: application/json" -d "{\"id\": 0, \"firstName\": \"firstName4\", \"lastName\": \"lastName4\", \"ipn\": \"4444444440\"}"
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons"
echo -e
curl -u admin:admin -X POST --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons" -H "Content-Type: application/json" -d "{\"id\": 0, \"firstName\": \"firstName1\", \"lastName\": \"lastName1\", \"ipn\": \"1111111111\"}"
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons/2"
echo -e
curl -u admin:admin -X PUT --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons/1" -H "Content-Type: application/json" -d "{\"id\": 0, \"firstName\": \"firstName10\", \"lastName\": \"lastName10\", \"ipn\": \"4444444440\"}"
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons"
echo -e
curl -u admin:admin -X DELETE --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons/1"
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons"
echo -e
curl -u admin:admin -X DELETE --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons"
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/persons"
echo -e
echo -e
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/tasks"
echo -e
curl -u admin:admin -X POST --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/tasks" -H "Content-Type: application/json" -d "{\"name\": \"task 1\",\"dateTime\": \"2023-01-06T12:26:44.28\",\"status\": \"PLANNED\"}"
echo -e
curl -u admin:admin -X POST --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/tasks" -H "Content-Type: application/json" -d "{\"name\": \"task 2\",\"dateTime\": \"2023-01-06T12:26:44.28\",\"status\": \"WORK_IN_PROGRESS\"}"
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/tasks/1"
echo -e
curl -u admin:admin -X PUT --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/tasks" -H "Content-Type: application/json" -d "{\"name\": \"task 1 changed\",\"dateTime\": \"2023-01-06T12:26:44.28\",\"status\": \"DONE\"}"
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/tasks/1"
echo -e
curl -u admin:admin -X GET --location "http://myrestapi-env.eba-9hkvqfd9.eu-central-1.elasticbeanstalk.com/api/tasks/1"
