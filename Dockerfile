FROM openjdk:11
COPY target/task8-3.0.0.jar prometeus-grafana.jar
ENTRYPOINT ["java","-jar","prometeus-grafana.jar"]