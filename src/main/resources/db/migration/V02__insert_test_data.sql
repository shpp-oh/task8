INSERT INTO person(id, first_name, last_name, ipn) VALUES (1, 'first name test1','last name test1', '1111111118');
INSERT INTO person(id, first_name, last_name, ipn) VALUES (2, 'first name test2','last name test2', '2222222225');
INSERT INTO person(id, first_name, last_name, ipn) VALUES (3, 'first name test3','last name test3', '3333333332');
INSERT INTO person(id, first_name, last_name, ipn) VALUES (4, 'first name test4','last name test4', '4444444440');

INSERT INTO task(id, name, date_time, status) VALUES (1, 'name test1',now,'PLANNED');
INSERT INTO task(id, name, date_time, status) VALUES (2, 'name test2',now,'WORK_IN_PROGRESS');
INSERT INTO task(id, name, date_time, status) VALUES (3, 'name test3',now,'DONE');
