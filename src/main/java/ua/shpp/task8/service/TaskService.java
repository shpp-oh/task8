package ua.shpp.task8.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import ua.shpp.task8.model.Task;
import ua.shpp.task8.model.TaskDto;
import ua.shpp.task8.repository.TaskRepository;
import ua.shpp.task8.util.StatusCheck;

import javax.validation.ValidationException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final MessageSource messageSource;

    @Autowired
    public TaskService(TaskRepository taskRepository, MessageSource messageSource) {
        this.taskRepository = taskRepository;
        this.messageSource = messageSource;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findById(Long id) {
        Optional<Task> taskData = taskRepository.findById(id);
        if (taskData.isPresent()) {
            return taskData.get();
        }
        String errorCode = "my.error.task.notfound";
        throw new NotFoundException(errorCodeMessage(errorCode) + messageSource
                .getMessage(errorCode, null, LocaleContextHolder.getLocale()));
    }

    public Task create(TaskDto taskDto) {
        return taskRepository.save(new Task(0L, taskDto.getName(), taskDto.getDateTime(), taskDto.getStatus()));
    }

    public Task update(Long id, TaskDto taskDto, Locale locale) {
        Optional<Task> taskData = taskRepository.findById(id);
        if (taskData.isPresent()) {
            Task updatedTask = taskData.get();
            if (!StatusCheck.isPossibleNewStatus(updatedTask, taskDto.getStatus())) {
                String errorCode = "my.error.status.wrong";
                throw new ValidationException(errorCodeMessage(errorCode) + messageSource
                        .getMessage(errorCode, null, locale) + ": "
                        + updatedTask.getStatus() + " -> " + taskDto.getStatus());
            }
            updatedTask.setName(taskDto.getName());
            updatedTask.setDateTime(taskDto.getDateTime());
            updatedTask.setStatus(taskDto.getStatus());
            return taskRepository.save(updatedTask);
        }
        String errorCode = "my.error.task.notfound";
        throw new NotFoundException(errorCodeMessage(errorCode) + messageSource
                .getMessage(errorCode, null, LocaleContextHolder.getLocale()));
    }

    public void deleteTask(Long id) {
        taskRepository.deleteById(id);
    }

    private String errorCodeMessage(String errorCode) {
        return String.format("Error code: %s: ", errorCode);
    }
}
