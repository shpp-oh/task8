package ua.shpp.task8.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.shpp.task8.model.TaskDto;
import ua.shpp.task8.service.TaskService;

import javax.validation.Valid;
import java.util.Locale;

@RestController
@RequestMapping("/api/tasks")
@SecurityRequirement(name = "${springdoc.api-docs.path}")
@Tag(name = "Task controller", description = "The controller allows you to work with your tasks. " +
        "Read more about its capabilities below.")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    @Operation(summary = "get all", description = "get all tasks info")
    public ResponseEntity<Object> getAllTasks() {
        return ResponseEntity.ok(taskService.findAll());
    }

    @GetMapping("/{id}")
    @Operation(summary = "get by ID", description = "get task info by ID")
    public ResponseEntity<Object> getTaskById(@PathVariable Long id) {
        return ResponseEntity.ok(taskService.findById(id));
    }

    @PostMapping
    @Operation(summary = "create", description = "create new task")
    public ResponseEntity<Object> createTask(@Valid @RequestBody TaskDto taskDto) {
        return ResponseEntity.ok(taskService.create(taskDto));
    }

    @PutMapping("/{id}")
    @Operation(summary = "update by ID", description = "update task by ID")
    public ResponseEntity<Object> updateTask(@PathVariable Long id, @Valid @RequestBody TaskDto taskDto, Locale locale) {
        return ResponseEntity.ok(taskService.update(id, taskDto, locale));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "delete by ID", description = "delete task by ID")
    public ResponseEntity<Object> deleteTask(@PathVariable Long id) {
        taskService.deleteTask(id);
        return ResponseEntity.ok().build();
    }

}
