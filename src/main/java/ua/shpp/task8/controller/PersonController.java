package ua.shpp.task8.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import ua.shpp.task8.model.Person;
import ua.shpp.task8.repository.PersonRepository;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/persons")
@SecurityRequirement(name = "${springdoc.api-docs.path}")
@Tag(name = "Person controller", description = "The controller allows you to work with your persons. " +
        "Read more about its capabilities below.")
public class PersonController {

    private final PersonRepository personRepository;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping()
    @Operation(summary = "Get all persons", description = "Getting all persons from app")
    public ResponseEntity<List<Person>> getAllPersons() {
        List<Person> persons = personRepository.findAll();
        return new ResponseEntity<>(persons, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get person", description = "Getting the person from app by ID")
    public ResponseEntity<Person> getPersonById(@PathVariable("id") Long id) {
        Optional<Person> personData = personRepository.findById(id);
        if (personData.isPresent()) {
            return new ResponseEntity<>(personData.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    @Operation(summary = "Post person", description = "Publish the person to an app using JSON")
    public ResponseEntity<Object> createPerson(@Valid @RequestBody Person person) {
        Optional<Person> taskData = personRepository.findById(person.getId());
        if (taskData.isPresent()) {
            throw new NotFoundException("The person is already present!");
        }
        Person newPerson = personRepository.save(person);
        return new ResponseEntity<>(newPerson, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update person", description = "Updating the person in the app by ID")
    public ResponseEntity<Person> updatePerson(@PathVariable("id") Long id, @Valid @RequestBody Person person) {
        Optional<Person> personData = personRepository.findById(id);
        if (personData.isPresent()) {
            Person updatedPerson = personData.get();
            updatedPerson.setFirstName(person.getFirstName());
            updatedPerson.setLastName(person.getLastName());
            updatedPerson.setIpn(person.getIpn());
            return new ResponseEntity<>(personRepository.save(updatedPerson), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete person", description = "Deleting person from app by ID")
    public ResponseEntity<Person> deletePersonById(@PathVariable("id") Long id) {
        try {
            personRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping
    @Operation(summary = "Delete all persons", description = "Deleting all persons from app")
    public ResponseEntity<Person> deleteAllPersons() {
        try {
            personRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
