package ua.shpp.task8.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ua.shpp.task8.model.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

}
