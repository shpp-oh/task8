package ua.shpp.task8.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.shpp.task8.model.Task;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
}
