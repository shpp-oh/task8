package ua.shpp.task8;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(version = "${my.api.version}",
        title = "${my.api.title}", description = "${my.api.description}"))
public class Task8Application {

    public static void main(String[] args) {
        SpringApplication.run(Task8Application.class, args);
    }

}
