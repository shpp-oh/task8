package ua.shpp.task8.model;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Schema(name = "Task DTO", description = "This DTO for API transfer")
public class TaskDto {

    @NotNull(message = "{my.error.task.notnull}")
    String name;

    @NotNull(message = "{my.error.task.notnull}")
    LocalDateTime dateTime;
    @NotNull(message = "{my.error.task.notnull}")
    Status status;

    public TaskDto() {
    }

    public TaskDto(String name, LocalDateTime dateTime, Status status) {
        this.name = name;
        this.dateTime = dateTime;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
