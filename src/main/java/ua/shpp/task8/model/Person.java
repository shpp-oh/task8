package ua.shpp.task8.model;

import ua.shpp.task8.util.CheckIpnInterface;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    String firstName;
    String lastName;

    @Size(min = 10, max = 10, message = "Size must be 10 numbers")
    @CheckIpnInterface
    String ipn;

    public Person() {
    }

    public Person(String firstName, String lastName, String ipn) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.ipn = ipn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIpn() {
        return ipn;
    }

    public void setIpn(String ipn) {
        this.ipn = ipn;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", ipn='" + ipn + '\'' +
                '}';
    }

}
