package ua.shpp.task8.model;

public enum Status {
    PLANNED(1, false),
    WORK_IN_PROGRESS(2, false),
    DONE(3, true),
    CANCELLED_DONE(4, true),
    CANCELLED(5, true);

    private final int order;
    private final boolean isFinal;

    Status(int order, boolean isFinal) {
        this.order = order;
        this.isFinal = isFinal;
    }

    public int getOrder() {
        return order;
    }

    public boolean isFinal() {
        return isFinal;
    }
}
