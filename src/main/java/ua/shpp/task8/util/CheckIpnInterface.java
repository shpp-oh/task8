package ua.shpp.task8.util;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CheckIpn.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckIpnInterface {
    String message() default "Wrong IPN: check sum is wrong";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}

