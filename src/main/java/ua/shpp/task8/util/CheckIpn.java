package ua.shpp.task8.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class CheckIpn implements ConstraintValidator<CheckIpnInterface, String> {

    public static final int SIZE = 10;

    @Override
    public void initialize(CheckIpnInterface ipn) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null || s.length() != SIZE) {
            return false;
        }

        List<Integer> list = new ArrayList<>();
        s.chars().filter(Character::isDigit)
                .map(Character::getNumericValue)
                .forEach(list::add);

        return checkSum(list);
    }

    private static boolean checkSum(List<Integer> list) {
        int[] multipliers = {-1, 5, 7, 9, 4, 6, 10, 5, 7};
        if (list.size() == SIZE) {
            int x = 0;
            for (int i = 0; i < 9; i++) {
                x += list.get(i) * multipliers[i];
            }
            return list.get(9) == (x % 11) % 10;
        } else {
            return false;
        }
    }

}