package ua.shpp.task8.util;

import ua.shpp.task8.model.Status;
import ua.shpp.task8.model.Task;

public class StatusCheck {

    public static boolean isPossibleNewStatus(Task task, Status newStatus) {
        return (!task.getStatus().isFinal() && newStatus != null)
                && (task.getStatus().getOrder() <= newStatus.getOrder());
    }
}