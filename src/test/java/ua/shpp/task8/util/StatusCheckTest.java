package ua.shpp.task8.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import ua.shpp.task8.model.Status;
import ua.shpp.task8.model.Task;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StatusCheckTest {

    Task taskPlanned, taskWorkInProgress, taskDone, taskCancelledDone, taskCancelled;

    @BeforeEach
    void setUpEach() {
        taskPlanned = new Task(1L, "Task PLANNED", LocalDateTime.now(),
                Status.PLANNED);
        taskWorkInProgress = new Task(2L, "Task WORK_IN_PROGRESS", LocalDateTime.now(),
                Status.WORK_IN_PROGRESS);
        taskDone = new Task(3L, "Task DONE", LocalDateTime.now(),
                Status.DONE);
        taskCancelledDone = new Task(4L, "Task CANCELLED_DONE", LocalDateTime.now(),
                Status.CANCELLED_DONE);
        taskCancelled = new Task(5L, "Task CANCELLED", LocalDateTime.now(),
                Status.CANCELLED);
    }

    @ParameterizedTest
    @MethodSource("argsForPlanned")
    void testValidTaskPlanned(Status newStatus) {
        assertTrue(StatusCheck.isPossibleNewStatus(taskPlanned, newStatus));
    }

    @ParameterizedTest
    @MethodSource("argForWorkInProgress")
    void testValidTaskWorkInProgress(Status newStatus) {
        assertTrue(StatusCheck.isPossibleNewStatus(taskWorkInProgress, newStatus));
    }

    @ParameterizedTest
    @MethodSource("argsForPlanned")
    void testInvalidFinalTaskDone(Status newStatus) {
        assertFalse(StatusCheck.isPossibleNewStatus(taskDone, newStatus));
    }

    @ParameterizedTest
    @MethodSource("argsForPlanned")
    void testInvalidFinalTaskCancelledDone(Status newStatus) {
        assertFalse(StatusCheck.isPossibleNewStatus(taskCancelledDone, newStatus));
    }

    @ParameterizedTest
    @MethodSource("argsForPlanned")
    void testInvalidFinalTaskCancelled(Status newStatus) {
        assertFalse(StatusCheck.isPossibleNewStatus(taskCancelled, newStatus));
    }

    @Test
    void testInvalidTaskWorkInProgressTo() {
        assertFalse(StatusCheck.isPossibleNewStatus(taskWorkInProgress, Status.PLANNED));
    }

    static Stream<Status> argsForPlanned() {
        return Stream.of(
                Status.PLANNED,
                Status.WORK_IN_PROGRESS,
                Status.DONE,
                Status.CANCELLED_DONE,
                Status.CANCELLED);
    }

    static Stream<Status> argForWorkInProgress() {
        return Stream.of(
                Status.WORK_IN_PROGRESS,
                Status.DONE,
                Status.CANCELLED_DONE,
                Status.CANCELLED);
    }

}

