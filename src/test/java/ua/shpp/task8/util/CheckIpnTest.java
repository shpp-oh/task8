package ua.shpp.task8.util;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ua.shpp.task8.model.Person;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CheckIpnTest {
    private static final CheckIpn checkIpn = new CheckIpn();

    @ParameterizedTest
    @CsvSource({"1111111118", "2222222225", "3333333332", "4444444440"})
    void isValid(String ipn) {
        assertTrue(checkIpn.isValid(ipn, null));
    }

    @ParameterizedTest
    @CsvSource({"xxx111111", "111 111 18", "1", "111111111", "1111111111", "2222222222", "3333333333", "4444444444"})
    void isNotValid(String ipn) {
        assertFalse(checkIpn.isValid(ipn, null));
    }

    @ParameterizedTest
    @CsvSource({"1", "1111111111"})
    void isValidationErrorMessagePresent(String ipn) {
        boolean result;
        Person invalidPerson = new Person("1", "2", ipn);
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Person>> errors = validator.validate(invalidPerson);
        if (ipn.length() != CheckIpn.SIZE) {
            result = errors.stream().anyMatch(error -> error.getMessage().equals("Size must be 10 numbers"));
        } else {
            result = errors.stream().anyMatch(error -> error.getMessage().equals("Wrong IPN: check sum is wrong"));
        }
        assertTrue(result);
    }

}