package ua.shpp.task8.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import ua.shpp.task8.model.Status;
import ua.shpp.task8.model.TaskDto;
import ua.shpp.task8.repository.TaskRepository;
import ua.shpp.task8.service.TaskService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Disabled
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
class TaskControllerMockIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ObjectMapper objectMapper;


    private static final List<TaskDto> taskList = new ArrayList<>();

    @BeforeAll
    static void beforeAll() {
        taskList.add(new TaskDto("Task 1", LocalDateTime.now(), Status.PLANNED));
        taskList.add(new TaskDto("Task 2", LocalDateTime.now(), Status.WORK_IN_PROGRESS));
        taskList.add(new TaskDto("Task 3", LocalDateTime.now(), Status.CANCELLED_DONE));
        taskList.add(new TaskDto("Task 4", LocalDateTime.now(), Status.DONE));
        taskList.add(new TaskDto("Task 5", LocalDateTime.now(), Status.CANCELLED));
    }

    @Order(1)
    @Test
    void create() throws Exception {
        taskList.forEach((t) -> taskService.create(t));

        TaskDto taskDto = taskList.get(0);

        ResultActions response = mockMvc.perform(post("/api/tasks")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(taskDto)));

        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(taskDto.getName())))
//                .andExpect(jsonPath("$.dateTime", is(taskDto.getDateTime())))
                .andExpect(jsonPath("$.status", is(taskDto.getStatus().toString())));
    }

    @Order(2)
    @Test
    void update() throws Exception {
        int id = 1;
        TaskDto taskDto = taskList.get(id - 1);
        taskDto.setName(taskDto.getName() + " updated");
        taskDto.setStatus(Status.WORK_IN_PROGRESS);

        ResultActions response = mockMvc.perform(put("/api/tasks/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(taskDto)));

        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(taskDto.getName())))
//                .andExpect(jsonPath("$.dateTime", is(taskDto.getDateTime())))
                .andExpect(jsonPath("$.status", is(taskDto.getStatus().toString())));
    }

    @Order(3)
    @Test
    void findAll() throws Exception {
        ResultActions response = mockMvc.perform(get("/api/tasks"));

        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.size()", is(taskRepository.findAll().size())));
    }

    @Order(4)
    @Test
    void findById() throws Exception {
        int id = taskList.size();
        TaskDto taskDto = taskList.get(id - 1);

        ResultActions response = mockMvc.perform(get("/api/tasks/{id}", id));

        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.name", is(taskDto.getName())))
//                .andExpect(jsonPath("$.dateTime", is(taskDto.getDateTime().toString())))
                .andExpect(jsonPath("$.status", is(taskDto.getStatus().toString())));

    }

    @Order(5)
    @Test
    void deleteTask() throws Exception {
        int id = taskList.size();
        ResultActions response = mockMvc.perform(delete("/api/tasks/{id}", id));

        response.andExpect(status().isOk())
                .andDo(print());

    }
}