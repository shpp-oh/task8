package ua.shpp.task8.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ua.shpp.task8.Task8Application;
import ua.shpp.task8.model.Status;
import ua.shpp.task8.model.Task;
import ua.shpp.task8.model.TaskDto;
import ua.shpp.task8.service.TaskService;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(classes = Task8Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TaskControllerIT {
    @LocalServerPort
    private int port;

    private static final String CLIENT_NAME = "admin";
    private static final String CLIENT_PASS = "admin";

    private static final TaskDto testValidTaskDto =
            new TaskDto("test task 1", LocalDateTime.now(), Status.PLANNED);
    private static final TaskDto testValidTaskDtoUpdated =
            new TaskDto("test task 1 updated", LocalDateTime.now(), Status.WORK_IN_PROGRESS);
    private static final TaskDto testNotValidTaskDto = new TaskDto();

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    TaskService taskService;


    HttpHeaders headers = new HttpHeaders();

    @Test
    @Order(1)
    void createTask() throws JSONException, JsonProcessingException {
        HttpEntity<TaskDto> entity = new HttpEntity<>(testValidTaskDto, headers);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth(CLIENT_NAME, CLIENT_PASS)
                        .exchange(createURLWithPort("/api/tasks"), HttpMethod.POST, entity, String.class);

        String expected = objectMapper.writeValueAsString(
                new Task(1L, testValidTaskDto.getName(),
                        testValidTaskDto.getDateTime(), testValidTaskDto.getStatus()));
        assertEquals(HttpStatus.OK, response.getStatusCode());
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    @Test
    @Order(2)
    void updateTask() throws JSONException, JsonProcessingException {
        HttpEntity<TaskDto> entity = new HttpEntity<>(testValidTaskDtoUpdated, headers);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth(CLIENT_NAME, CLIENT_PASS)
                        .exchange(createURLWithPort("/api/tasks/1"), HttpMethod.PUT, entity, String.class);

        String expected = objectMapper.writeValueAsString(
                taskService.update(1L, testValidTaskDtoUpdated, LocaleContextHolder.getLocale()));
        JSONAssert.assertEquals(expected, response.getBody(), false);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    @Order(3)
    void getAllTasks() throws JSONException, JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth(CLIENT_NAME, CLIENT_PASS)
                        .exchange(createURLWithPort("/api/tasks"), HttpMethod.GET, entity, String.class);

        String expected = objectMapper.writeValueAsString(
                taskService.findAll());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    @Test
    @Order(4)
    void getTaskById() throws JSONException, JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth(CLIENT_NAME, CLIENT_PASS)
                        .exchange(createURLWithPort("/api/tasks/1"), HttpMethod.GET, entity, String.class);

        String expected = objectMapper.writeValueAsString(
                taskService.findById(1L));

        assertEquals(HttpStatus.OK, response.getStatusCode());
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    @Test
    @Order(5)
    void deleteTask() throws JSONException {
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response =
                restTemplate.withBasicAuth(CLIENT_NAME, CLIENT_PASS)
                        .exchange(createURLWithPort("/api/tasks/1"), HttpMethod.GET, entity, String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}